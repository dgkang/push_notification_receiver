package config

import (
    "encoding/json"
    "io/ioutil"
)

/*
 * Config: exported struct
 * CONFIG: exported global var
 */

type (
    Config struct {
        Host string `json:"host"`
        LogFile string `json:"log_file"`
        LogLevel string `json:"log_level"`
        PidFile string `json:"pid_file"`
        Redis struct {
            Host string `json:"host"`
            Timeout float64 `json:"timeout"`
        } `json:"redis"`
        ConfigMysql struct {
            Host string `json:"host"`
            Port float64 `json:"port"`
            User string `json:"user"`
            Password string `json:"password"`
            Database string `json:"database"`
            Charset string `json:"charset"`
            Loc string `json:"loc"`
            Timeout float64 `json:"timeout"`
        } `json:"mysql"`
    }
)

var (
    CONFIG Config
)

func ConfigReload(path string) error {
    content, err := ioutil.ReadFile(path)
    if err != nil {
        return err
    }

    var tmp Config
    if err = json.Unmarshal(content, &tmp); err != nil {
        return err
    }
    CONFIG = tmp
    return nil
}
