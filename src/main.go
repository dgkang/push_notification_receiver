package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/fzzy/radix/redis"

	"config"
	"logger"
)

const (
	error_success = iota
	error_invalid_data
	error_redis
)

var (
	error_map_string = map[int]string{
		error_success:      "success",
		error_invalid_data: "invalid data",
		error_redis:        "inner error",
	}

	flag_config = flag.String("config", "", "config file path")

	redis_channel_number = runtime.NumCPU() * 10
	redis_channel        = make(chan *redis.Client, redis_channel_number)

	redis_key_event_push_notification  = "event.push_notification" //right push and left pop
	redis_last_push_notification_count = 0
)

func main() {
	defer logger.LogClose()

	// init terminal args
	flag.Parse()
	if *flag_config == "" {
		flag.Usage()
		return
	}

	// init rand seed
	rand.Seed(int64(os.Getpid()) ^ time.Now().Unix())

	// init redis client pool via channel
	for i := 0; i < redis_channel_number; i++ {
		redis_channel <- nil
	}

	// init config
	var err error
	err = config.ConfigReload(*flag_config)
	if err != nil {
		logger.Log(logger.LOG_LEVEL_ERROR, err)
		return
	}
	logger.Log(logger.LOG_LEVEL_DEBUG, config.CONFIG)

	// init log
	err = logger.LogReload(config.CONFIG.LogFile, config.CONFIG.LogLevel)
	if err != nil {
		logger.Log(logger.LOG_LEVEL_ERROR, err)
		return
	}

	// write pid
	if err = writePIDFile(); err != nil {
		logger.Log(logger.LOG_LEVEL_ERROR, err)
		return
	}

	// init http server
	logger.Log(logger.LOG_LEVEL_INFO, "push notification start...")
	go func() {
		http.HandleFunc("/api/1.0/push_notification", pushNotification)
		if err = http.ListenAndServe(config.CONFIG.Host, nil); err != nil {
			logger.Log(logger.LOG_LEVEL_ERROR, err)
		}
	}()
	handleSignal()
}

func handleSignal() {
	signal_chan := make(chan os.Signal, 1)
	signal.Notify(signal_chan, []os.Signal{syscall.SIGHUP, syscall.SIGUSR1}...)
	for {
		s := <-signal_chan
		logger.Log(logger.LOG_LEVEL_INFO, "signal:", s)

		switch s {
		case syscall.SIGPIPE:
		case syscall.SIGHUP:
			if err := config.ConfigReload(*flag_config); err != nil {
				logger.Log(logger.LOG_LEVEL_ERROR, err)
			} else {
				logger.Log(logger.LOG_LEVEL_INFO, "reload config success")
			}
		case syscall.SIGTERM:
			logger.Log(logger.LOG_LEVEL_WARNING, "push notification stop...")
			return
		case syscall.SIGINT:
		case syscall.SIGUSR1:
			if err := logger.LogReload(config.CONFIG.LogFile, config.CONFIG.LogLevel); err != nil {
				logger.Log(logger.LOG_LEVEL_ERROR, err)
			} else {
				logger.Log(logger.LOG_LEVEL_INFO, "reload log success")
			}
		case syscall.SIGUSR2:
		default:
		}
	}
}

func writePIDFile() error {
	pid := fmt.Sprintf("%d", os.Getpid())
	return ioutil.WriteFile(config.CONFIG.PidFile, []byte(pid), 0644)
}

func redisCommand(cmd string, args ...interface{}) *redis.Reply {
	client := <-redis_channel
	defer func() {
		redis_channel <- client
	}()

	if client == nil {
		var err error
		client, err = redis.DialTimeout("tcp", config.CONFIG.Redis.Host, time.Duration(config.CONFIG.Redis.Timeout)*time.Millisecond)
		if err != nil {
			logger.Log(logger.LOG_LEVEL_ERROR, err)
			return nil
		}
	}

	reply := client.Cmd(cmd, args...)
	if reply.Err != nil {
		logger.Log(logger.LOG_LEVEL_ERROR, reply.Err)
		client.Close() // already called in Cmd() if reply error, close again for safe
		client = nil
		return nil
	}
	return reply
}

func getErrorReply(error_code int) map[string]interface{} {
	m := map[string]interface{}{
		"error_code": error_code,
		"err":        error_map_string[error_code],
	}
	return m
}

func writeErrorReply(w http.ResponseWriter, m map[string]interface{}) {
	b, _ := json.Marshal(m)
	fmt.Fprintln(w, string(b))
}

func pushNotification(w http.ResponseWriter, r *http.Request) {
	// read http body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.Log(logger.LOG_LEVEL_ERROR, err)
		writeErrorReply(w, getErrorReply(error_invalid_data))
		return
	}

	// check validity of the fields
	type Data struct {
		PlatForm    string `json:"platform"`
		Enviroment  string `json:"enviroment"`
		AppName     string `json:"app_name"`
		DeviceToken string `json:"device_token"`
		Payload     struct {
			Aps struct {
				Alert string  `json:"alert"`
				Badge float64 `json:"badge"`
				Sound string  `json:"sound"`
			} `json:"aps"`
		} `json:"payload"`
	}

	type Batch struct {
		PlatForm     string   `json:"platform"`
		Enviroment   string   `json:"enviroment"`
		AppName      string   `json:"app_name"`
		Alert        string   `json:"alert"`
		Badge        float64  `json:"badge"`
		Sound        string   `json:"sound"`
		DeviceTokens []string `json:"device_tokens"`
	}

	var batch Batch
	err = json.Unmarshal(body, &batch)
	if err != nil {
		logger.Log(logger.LOG_LEVEL_DEBUG, err)
		writeErrorReply(w, getErrorReply(error_invalid_data))
		return
	}

	for _, v := range batch.DeviceTokens {
		data := &Data{}
		data.PlatForm = batch.PlatForm
		data.Enviroment = batch.Enviroment
		data.AppName = batch.AppName
		data.DeviceToken = v
		data.Payload.Aps.Alert = batch.Alert
		data.Payload.Aps.Badge = batch.Badge
		data.Payload.Aps.Sound = batch.Sound

		payload, err := json.Marshal(data.Payload)
		if err != nil {
			logger.Log(logger.LOG_LEVEL_DEBUG, err)
			writeErrorReply(w, getErrorReply(error_invalid_data))
			return
		}

		jsData, err := json.Marshal(data)
		if err != nil {
			logger.Log(logger.LOG_LEVEL_DEBUG, err)
			writeErrorReply(w, getErrorReply(error_invalid_data))
			return
		}

		if (data.PlatForm != "ios") ||
			(data.Enviroment != "sandbox" && data.Enviroment != "production") ||
			(len(data.DeviceToken) != 64) ||
			(len(payload) > 256) {
			logger.Log(logger.LOG_LEVEL_INFO, "invalid data format", string(jsData))
			writeErrorReply(w, getErrorReply(error_invalid_data))
			return
		}

		// write to redis
		reply := redisCommand("rpush", redis_key_event_push_notification, string(jsData))
		if reply == nil {
			writeErrorReply(w, getErrorReply(error_redis))
			return
		}

		ret, err := reply.Int()
		if err != nil {
			logger.Log(logger.LOG_LEVEL_ERROR, err)
			writeErrorReply(w, getErrorReply(error_redis))
			return
		}
		if ret-redis_last_push_notification_count >= 1000 { // log for each increase of 1000
			redis_last_push_notification_count = ret
			logger.Log(logger.LOG_LEVEL_WARNING, "push notification increased to", ret)
		}
	}
	writeErrorReply(w, getErrorReply(error_success))
}
